<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCreated extends Mailable
{
    use Queueable, SerializesModels;


    public $order;
    public $products;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order =$order;
        $this->products = $order-> shopping_cart->products()->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */

  /*   https://stackoverflow.com/questions/42558903/expected-response-code-250-but-got-code-535-with-message-535-5-7-8-username */
  /*   mira para que funcione el correo  */
  public function build()
    {
        return $this->from("kevinnivek44@gmail.com")
                        ->view('mailers.order');
    }
}
