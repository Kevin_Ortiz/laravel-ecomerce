<?php
namespace App;

use Illuminate\Queue\Console\RetryCommand;
use PhpParser\Node\Stmt\TryCatch;

class PayPal
{
    private $_apiContext;
    private $shopping_cart;
    private $_ClientId ='AaCA5cQMjIJlFBRFRIwjOllEu3y327HxaNJpDLRrVAv5UCoknKB7AnPOEwpMUFpsjtzJcb6mh_twFemN';
    private $_ClienSecret = 'EBy6nl5gdFErJjvV0DDcOk7cf88fRgVW1b2J_pngf9huV1ontJlFddOICDH4RCW47UEg1ZNVDKJcDpsF';

    public function __construct($shopping_cart)
    {
        $this->_apiContext= \PaypalPayment::ApiContext($this->_ClientId, $this->_ClienSecret);
        $config = config("paypal_payment");
        $flatConfig = array_dot($config);

        $this->_apiContext->setConfig($flatConfig);
        $this->shopping_cart = $shopping_cart;
    }
    
    public function generate()
    {
        $payment =\PaypalPayment::payment()->setIntent("sale")
         -> setPayer($this->payer()) 
         ->setTransactions([$this->transaction()])
         ->setRedirectUrls($this->redirectURLs());

         try{
            $payment->create($this->_apiContext);
         }catch(\Exception $ex){
            dd($ex);
            exit(1);
         }
         return $payment;
    }

    public function payer()
    //return payment's info
    {
        return \PaypalPayment::payer()
                                ->setPaymentMethod("paypal");
    }

    public function redirectURLs()
    //return transaction's info
    {
        $baseURL = url('/');
        return \PaypalPayment::redirectUrls()
                                ->setReturnUrl("$baseURL/payments/store")
                                ->setCancelUrl("$baseURL/carrito");
    }

    public function transaction()
    //return transaction's info
    {
        return \PaypalPayment::transaction()
                            ->setAmount($this->amount())
                            ->setItemList($this->items())
                            ->setDescription("Tu compra en Productos@Krissvera")
                            ->setInvoiceNumber(uniqid());
    }

    public function items()
    {
        $items =[];
        $products = $this->shopping_cart->products()->get();
        foreach ($products as $product){
            array_push($items, $product->paypalItem());
        }
        return \PaypalPayment::itemList()->setItems($items);
    }

    public function amount()
    {
        return \PaypalPayment::amount()->setCurrency("USD")
                            ->setTotal($this->shopping_cart->totalUSD());
    }

   
    public function execute($paymentId, $payerId){

        $payment = \PaypalPayment::getById($paymentId,$this->_apiContext);

        $execution = \PaypalPayment::PaymentExecution()
                                    ->setPayerId($payerId);

      return  $payment->execute($execution, $this->_apiContext);

    }
        
    
}