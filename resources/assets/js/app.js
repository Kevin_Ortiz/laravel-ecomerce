$.fn.editable.defaults.mode = 'inline';
$.fn.editable.defaults.ajaxOptions = {type: "PUT"}; 

$(document).ready(function(){
   
    
     $('.set-guide-number').editable();

    $('.select-status').editable({
        source: [
            {value: "creado", text: "Creado" },
            {value: "enviado", text: "Enviado" },
            {value: "Recibido", text: "Recibido" }
        ]
    }); 

    $(".add-to-cart").on("submit", function(ev){
        ev.preventDefault();
        var $form = $(this);
        var $button = $form.find("[type='submit']");
        //ajax
        $.ajax({
            url: $form.attr("action"),
            method: $form.attr("method"),
            data: $form.serialize(),
           dataType:"JSON",
            beforeSend: function(){
                $button.val("Cargando....");
            },
            success: function(data){
                $button.css("background-color","#00c853").val("Agregado");

               // console.log(data);
                $(".circle-shopping-cart").html(data.products_count).addClass("highlight");
                setTimeout(function(){
                    restartButton($button);
                },2000);
            },
            error: function(){
                console.log(err);
                $button.css("background-color","#d50000").val("Hubo un ERROR.");

                setTimeout(function(){
                    restartButton($button);
                },2000);
            }
        });
        return false;
    });
    function restartButton($button){
        $button.val("Agregar al carrito").attr("style","");
        $(".circle-shopping-cart").remove("highlight");
    }
 });

 /*    $('.select-status').editable({
        url: url,
       
        type:"select",
        source: [
        {value: creado, text: 'Creado'},
        {value: enviado, text: 'Enviado'},
        {value: Recibido, text: 'Recibido'},
        {value: 4, text: '4 Star'},
        {value: 5, text: '5 Star'}
        ],
        validate:function(value){
          if($.trim(value) === '')
          {
            return 'This field is required';
          }
        }
      });
}); 
  */

/*  $(document).ready(function () {
    $ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}'
        }
    });

    $('.select-status').editable({
        url: '{{url("/orders/$order->id")}}',
        title: 'Update',
        success: function (response, newValue) {
            console.log('Updated', response)
        }
    });
})  */