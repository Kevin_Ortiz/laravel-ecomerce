@extends('layouts.app')

<link href="https://cdn.jsdelivr.net/gh/Talv/x-editable@develop/dist/bootstrap4-editable/css/bootstrap-editable.css" rel="stylesheet">
<link href="https://unpkg.com/bootstrap-table@1.15.4/dist/bootstrap-table.min.css" rel="stylesheet">


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2>Dashboard</h2>
            </div>
            <div class="panel-body">
                <h3>Estadistcas</h3>

                <div class="row top-space">
                    <div class="col-xs-4 col-md-3 col-lg-2 sale-data">
                    <span>{{$totalMonth}} USD</span>
                        Ingresos del mes
                    </div>

                    <div class="col-xs-4 col-md-3 col-lg-2 sale-data">
                        <span>{{$totalMonthCount}}</span>
                            Numero de ventas
                        </div>
                    
                </div>

                <h3>Ventas</h3>
                <table class="table table-bordered">
                
                    <thead>
                        <tr>
                            <td>ID. Venta</td>
                            <td>Comprador</td>
                            <td>Direccion</td>
                            <td>No. guia</td>
                            <td>Status</td>
                            <td>Fecha de venta</td>
                            <td>Acciones</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->recipient_name}}</td>
                                <td>{{$order->address()}}</td>
                                <td>
                                    <a href="#" 
                                    data-type="text" 
                                    data-pk="{{$order->id}}"
                                    data-url="{{url("/orders/$order->id")}}"
                                    data-title="Numero de guia"
                                    data-value="{{$order->guide_number}}"
                                    class="set-guide-number"
                                    data-name="guide_number"></a>
                                </td>
                                <td>
                                    <a href="#" 
                                   
                                    data-type="select" 
                                    data-pk="{{ $order->id }}"
                                    data-url="{{url("/orders/$order->id")}}"
                                    data-title="Numero de guia"
                                    data-value="{{$order->status}}"
                                    class="select-status"
                                    data-name="status">{{$order->status}}</a>


                                 
                                </td>
                                <td>{{$order->created_at}}</td>
                                <td>Acciones</td>
                            </tr>
                            
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- <script >
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $('.set-guide-number').editable({
                url:'{{url("/orders/$order->id")}}'
                title: 'Update',
                success:function (response,newValue){
                    console.log('Updated',response)
                }
            });
        })
    </script> --}}
 @endsection

