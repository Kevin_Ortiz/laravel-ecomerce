<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <title>Correo</title>
</head>
<body>
    <h1>Hola!</h1>
    <p>Estos son Los Productos de tu Compra Realizada en: Tienda@KrissVera</p>
    <table>
        <thead>
            <tr>
                <th>Productos</th>
                <th>Costo</th>
            </tr>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>{{$product->title}}</td>
                    <td>{{$product->pricing}}</td>
                </tr>
            @endforeach
               <tr>
                     <td>Total</td>
                     <td>{{$order->total}}</td>
             </tr>
        </tbody>
        </thead>
        
    </table>
</body>
</html>