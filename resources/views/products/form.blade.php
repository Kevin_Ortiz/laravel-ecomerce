{!! Form::open(['url' => $url, 'method' => $method, 'files'=> true]) !!}

<div class="from-group">
{{ Form::text('title',$product->title,['class' => 'from-control', 'placeholder'=>'Titulo...']) }}
</div>

<div class="from-group">
{{ Form::number('pricing',$product->pricing,['class' => 'from-control', 'placeholder'=>'Precio de tu producto en centavos de dolar...']) }}
</div>

<div class="from-group">
    {{ form::file('cover') }}
    </div>

<div class="from-group">
{{ Form::textarea('description',$product->description,['class' => 'from-control', 'placeholder'=>'Describe tu producto...']) }}
</div>

<div class="from-group">
<a href="{{url('/products')}}"> Regresar al listado de productos</a>
<input type="submit" value="Enviar" class="btn btn-success">
</div>

{!! Form::close() !!}